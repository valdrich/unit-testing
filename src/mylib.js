// src/mylib.js
module.exports = {
    sum: (a, b) => a + b,
    mul: (m, n) => m * n,
    random: () => Math.random(),
    arrayGen: () => [1,2,3]
}