// src/main.js

const express = require('express')
const app = express()
const port = 3000
//importing math fuctions to the main.js
const mylib = require('./mylib')

//using the imported mylib functions
console.log({ 
    sum: mylib.sum(1, 1),
    mul: mylib.mul(2, 2),
    random: mylib.random(),
    array: mylib.arrayGen()
});

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})