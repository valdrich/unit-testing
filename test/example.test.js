// example.test.js
const expect = require('chai').expect
const should = require('chai').should()
const { assert } = require('chai')
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    //runs after all tests in this block
    after(() => console.log('After mylib tests'))

    //unit test for the mylib.sum function. Expect
    it('Should return 2 when using sum function with a=1, b=1',() => {
       const result = mylib.sum(1,1) // 1 + 1
       expect(result).to.equal(2) // result expected to equal 2
    })

    //unit test for the mylib.mul function. Expect
    it('Should return 4 when using mul function with m=2, n=2',() => {
        const result = mylib.mul(2,2) // 2 * 2
        expect(result).to.equal(4) // result expected to equal 4
     })     

    //run tasks before tests in this block
    before(() => {
        myvar = 1; //steup before testing
        console.log('Before testing')
    })

    it('Assert foo is not bar', () => {
        assert('foo' != 'bar')
    })

    it('Myvar should exist', () => {
        should.exist(myvar)
    })

    water = 'stone'

    it('Water has to be a string equal to stone and with the lenght of 5', () => {
        //should examples
        water.should.be.a('string');
        water.should.equal('stone');
        water.should.have.lengthOf(5);
        
        //expect examples
        expect(water).to.be.a('string');
        expect(water).to.equal('stone');
        expect(water).to.have.lengthOf(5);

        //assert examples
        assert.typeOf(water, 'string');
        assert.typeOf(water, 'string', 'water is a string');
        assert.equal(water, 'stone', 'water equal `stone`');
        assert.lengthOf(water, 5, 'water`s value has a length of 5');
    })

    //test will randomly fail
    //it('Random', () => expect(mylib.random()).to.above(0.5))
})